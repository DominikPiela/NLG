import math

import torch
from torch import nn
from torch import Tensor
# noinspection PyPep8Naming
from torch.nn import functional as F


class LanguageModel(nn.Module):
    def __init__(self, vocabulary_size: int, embedding_dimension: int, num_heads: int,
                 num_layers: int, context_size: int, dropout: float, device=None):
        super().__init__()
        self.embedding = nn.Embedding(vocabulary_size, embedding_dimension)
        self.positional = nn.Embedding(context_size, embedding_dimension)
        self.layers = nn.ModuleList([Layer(
            embedding_dimension=embedding_dimension,
            num_heads=num_heads,
            context_size=context_size,
            dropout=dropout,
            device=device) for _ in range(num_layers)])
        self.norm = nn.LayerNorm(embedding_dimension)
        self.linear = nn.Linear(embedding_dimension, vocabulary_size)
        self.device = device

    def forward(self, src: Tensor) -> Tensor:
        # noinspection PyPep8Naming
        # B. batch_size
        # C. context_size
        # E. embedding_dimension
        B, C = src.shape

        embedding = self.embedding(src)  # (B,C,E)
        positional = self.positional(torch.arange(C, device=self.device))  # (C,E)
        output = embedding + positional

        for layer in self.layers:
            output = layer(output)

        output = self.norm(output)
        output = self.linear(output)  # (B,C,E)
        return output


class Layer(nn.Module):
    def __init__(self, embedding_dimension: int, num_heads: int, context_size: int, dropout: float, device=None):
        super().__init__()
        head_dimension = embedding_dimension // num_heads
        assert head_dimension * num_heads == embedding_dimension, \
            f'embedding_dimension {embedding_dimension} not divisible by num_heads {num_heads}'

        # self-attention
        self.self_attn = MultiHeadAttention(
            embedding_dimension=embedding_dimension,
            num_heads=num_heads,
            head_dimension=head_dimension,
            context_size=context_size,
            dropout=dropout,
            device=device,
        )

        # feed forward
        self.linear1 = nn.Linear(embedding_dimension, embedding_dimension * 4)
        self.dropout = nn.Dropout(dropout)
        self.linear2 = nn.Linear(embedding_dimension * 4, embedding_dimension)

        self.norm1 = nn.LayerNorm(embedding_dimension)
        self.norm2 = nn.LayerNorm(embedding_dimension)
        self.dropout1 = nn.Dropout(dropout)
        self.dropout2 = nn.Dropout(dropout)

    def forward(self, x: Tensor) -> Tensor:
        x = x + self.norm1(self._sa_block(x))
        x = x + self.norm2(self._ff_block(x))
        return x

    # self-attention block
    def _sa_block(self, x: Tensor) -> Tensor:
        x = self.self_attn(x)
        return self.dropout1(x)

    # feed forward block
    def _ff_block(self, x: Tensor) -> Tensor:
        x = self.linear2(self.dropout(F.relu(self.linear1(x))))
        return self.dropout2(x)


class MultiHeadAttention(nn.Module):
    def __init__(self, embedding_dimension: int, num_heads: int, head_dimension: int,
                 context_size: int, dropout: float, device=None):
        super().__init__()
        self.heads = nn.ModuleList([SelfAttention(
            embedding_dimension=embedding_dimension,
            head_dimension=head_dimension,
            context_size=context_size,
            dropout=dropout,
            device=device,
        ) for _ in range(num_heads)])
        self.linear = nn.Linear(embedding_dimension, embedding_dimension)

    def forward(self, x: Tensor) -> Tensor:
        output = torch.cat([head(x) for head in self.heads], dim=-1)
        output = self.linear(output)
        return output


class SelfAttention(nn.Module):
    def __init__(self, embedding_dimension: int, head_dimension: int, context_size: int, dropout: float, device=None):
        super().__init__()
        self.K = nn.Linear(embedding_dimension, head_dimension, bias=False)
        self.Q = nn.Linear(embedding_dimension, head_dimension, bias=False)
        self.V = nn.Linear(embedding_dimension, head_dimension, bias=False)
        self.dropout = nn.Dropout(dropout)
        self.register_buffer('mask', self._generate_mask(size=context_size, device=device))

    def forward(self, x: Tensor) -> Tensor:
        # noinspection PyPep8Naming
        B, C, E = x.shape

        k = self.K(x)  # (B,C,E)
        q = self.Q(x)  # (B,C,E)
        matmul = q @ k.transpose(-2, -1)  # (B,C,E) @ (B,E,C) -> (B,C,C)
        scaled = matmul * (1.0 / math.sqrt(E))
        masked = scaled + self.mask[:C, :C]
        softmax = F.softmax(masked, dim=-1)
        softmax = self.dropout(softmax)  # (B,C,C)

        v = self.V(x)  # (B,C,E)
        output = softmax @ v  # (B,C,C) @ (B,C,E) -> (B,C,E)
        return output

    @staticmethod
    def _generate_mask(size: int, device='cpu') -> Tensor:
        return torch.triu(torch.full((size, size), float('-inf'), device=device), diagonal=1)
