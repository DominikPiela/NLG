import time

import torch
from torch import nn
# noinspection PyPep8Naming
from torch.nn import functional as F

import data
import util
# noinspection PyUnresolvedReferences
from data import Dataset, Vocabulary
from model import LanguageModel
from util import Config, checkpoint


def main():
    print(util.now(), end='\n\n')
    config = Config()
    print(config, end='\n\n')
    torch.manual_seed(config.seed)

    vocabulary = data.load_vocabulary(config=config)
    train_dataset, valid_dataset = data.load_train_valid_dataset(config=config, vocabulary=vocabulary)
    print(f'train_dataset.num_batches {train_dataset.num_batches}')

    model = LanguageModel(
        vocabulary_size=vocabulary.size,
        embedding_dimension=config.embedding_dimension,
        num_heads=config.num_heads,
        num_layers=config.num_layers,
        context_size=config.context_size,
        dropout=config.dropout,
        device=config.device,
    ).to(config.device)
    print(f'{sum(p.numel() for p in model.parameters())} parameters', end='\n\n')

    optimizer = torch.optim.AdamW(model.parameters(), lr=config.learning_rate)
    epoch = checkpoint.load(model=model, config=config)

    for i in range(epoch, config.num_epochs):
        print(f'{util.now()} epoch {i + 1:2d}/{config.num_epochs}')

        train(model=model, dataset=train_dataset, optimizer=optimizer, config=config)
        checkpoint.save(model=model, epoch=i + 1, config=config)
        valid_loss = evaluate(model=model, dataset=valid_dataset, config=config)
        print(f'valid loss {valid_loss:4.2f}')
        print(generate(model=model, config=config, vocabulary=vocabulary, text='je stem <> stu den tem .'), end='\n\n')


def train(model: nn.Module, dataset: Dataset, optimizer: torch.optim.Optimizer, config: Config) -> None:
    model.train()
    start_time = time.time()
    total_loss = 0.0

    for idx, x, y in dataset.batches():
        output = model(x)
        loss = config.criterion(output, y)

        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), config.gradient_clipping)
        optimizer.step()

        total_loss += loss.item()
        if idx % config.logging_interval == 0:
            elapsed_time = time.time() - start_time
            print(f'batch {idx:4d}/{dataset.num_batches} | '
                  f'time {elapsed_time * 1000 / config.logging_interval :4.2f} ms | '
                  f'loss {total_loss / config.logging_interval :4.2f}')
            start_time = time.time()
            total_loss = 0.0


def evaluate(model: nn.Module, dataset: Dataset, config: Config) -> float:
    model.eval()
    total_loss = 0.0

    with torch.no_grad():
        for idx, x, y in dataset.batches():
            output = model(x)
            loss = config.criterion(output, y)
            total_loss += loss.item()

    return total_loss / dataset.num_batches


def generate(model: nn.Module,
             config: Config,
             vocabulary: Vocabulary,
             text: str = '',
             new_tokens: int = 32) -> str:
    model.eval()

    if text == '':
        src = torch.zeros((1, 1), dtype=torch.long, device=config.device)
    else:
        print(text)
        encoded = vocabulary.encode(text)
        print(encoded)
        tensor = util.encoded2tensor(encoded)
        src = torch.unsqueeze(tensor, dim=0).to(config.device)

    for _ in range(new_tokens):
        last_context = src[:, -config.context_size:]
        with torch.no_grad():
            output = model(last_context)
        output = output[:, -1, :]
        probabilities = F.softmax(output, dim=-1)
        tgt = torch.multinomial(probabilities, num_samples=1)
        src = torch.cat((src, tgt), dim=1)

    encoded = src[0].tolist()
    print(encoded)
    decoded = vocabulary.decode(encoded)
    print(decoded)
    detokenized = util.detokenize(decoded)
    # print(detokenized)
    return detokenized


if __name__ == '__main__':
    main()
