import json
import os.path
from collections.abc import Iterator
from typing import Any

from torch import Tensor

import util
from util import Config, pkl


class Vocabulary:
    def __init__(self, filename: str):
        """
        :param filename: Filename for dictionary file (word occurrence\\n)
        student 2137\\n
        sport 1670\\n
        ...
        """
        self._words = ['<bos>', '<eos>']
        with open(filename, encoding='utf-8') as f:
            for line in f:
                self._words.append(line.strip().split()[0].strip())
        self.size = len(self._words)

    def encode_from_file(self, filename: str) -> list[int]:
        total_lines = sum(1 for _ in open(filename, encoding='utf-8'))

        with open(filename, encoding='utf-8') as f:
            output = []
            for idx, line in enumerate(f):
                if idx % 10000 == 0:
                    print(f'{idx} / {total_lines}')
                output.extend(self.encode(line.strip()))
            return output

    def encode(self, text: str) -> list[int]:
        return [self.get_index(word) for word in text.split()]

    def decode(self, encoded: list[int]) -> str:
        return ' '.join([self.get_value(idx) for idx in encoded])

    def get_index(self, word: str) -> int:
        try:
            return self._words.index(word)
        except ValueError:
            return self._words.index('α')

    def get_value(self, idx: int) -> str:
        if 0 <= idx < self.size:
            return self._words[idx]
        else:
            raise RuntimeError(f'{idx} out of vocabulary')


class Dataset:
    def __init__(self, filename: str, config: Config):
        self.filename = filename
        self.config = config
        self.num_batches = sum(1 for line in open(filename) if line.strip() != '')

    def batches(self) -> Iterator[tuple[int, Tensor, Tensor]]:
        with open(self.filename, encoding='utf-8') as f:
            idx = 0
            for line in f:
                line = line.strip()
                if line != '':
                    idx += 1
                    x, y = self.json_import(line)
                    x = util.encoded2tensor(x).to(self.config.device)
                    y = util.encoded2tensor(y).to(self.config.device)
                    yield idx, x, y

    @classmethod
    def tokenized2dataset(cls, vocabulary: Vocabulary, config: Config):
        batch_size = config.batch_size
        context_size = config.context_size
        tokenized = config.path.tokenized
        dataset = config.path.dataset

        with open(dataset, 'a', encoding='utf-8') as output_file:
            for batch in cls.get_batches(
                    input_filename=tokenized,
                    batch_size=batch_size,
                    context_size=context_size,
                    vocabulary=vocabulary):
                output_file.write(cls.json_export(batch) + '\n')

    @classmethod
    def get_batches(cls, input_filename: str, batch_size: int, context_size: int, vocabulary: Vocabulary):
        size = batch_size * context_size
        batches_x = cls.encoded_batches_x_from_tokenized_txt(filename=input_filename, size=size, vocabulary=vocabulary)

        batch_x = next(batches_x, None)
        next_batch_x = next(batches_x, None)

        while not (batch_x is None or next_batch_x is None):
            x = cls.view(encoded=batch_x, batch_size=batch_size, context_size=context_size)
            y = cls.view(encoded=batch_x[1:] + [next_batch_x[0]], batch_size=batch_size, context_size=context_size)
            yield x, y

            batch_x = next_batch_x
            next_batch_x = next(batches_x, None)

    @staticmethod
    def view(encoded: list[int], batch_size: int, context_size: int) -> list[list[int]]:
        return [encoded[i * context_size : (i + 1) * context_size] for i in range(batch_size)]

    @classmethod
    def encoded_batches_x_from_tokenized_txt(
            cls,
            filename: str,
            size: int,
            vocabulary: Vocabulary) -> Iterator[list[int]]:
        with open(filename, encoding='utf-8') as input_file:
            tokens = []
            for line in input_file:
                tokens.extend(line.strip().split())
                while len(tokens) >= size:
                    batch = tokens[:size]
                    tokens = tokens[size:]
                    yield [vocabulary.get_index(token) for token in batch]

    @staticmethod
    def json_export(data: Any) -> str:
        return json.dumps(data, separators=(',', ':'))

    @staticmethod
    def json_import(json_string: str) -> Any:
        return json.loads(json_string)

    @staticmethod
    def split_train_valid(dataset_filename: str, train_filename: str, valid_filename: str):
        with (open(dataset_filename, encoding='utf-8') as dataset_file,
              open(train_filename, 'w', encoding='utf-8') as train_file,
              open(valid_filename, 'w', encoding='utf-8') as valid_file):
            for i, line in enumerate(dataset_file):
                line = line.strip()
                if i % 10 != 0:
                    train_file.write(line + '\n')
                else:
                    valid_file.write(line + '\n')


def load_vocabulary(config: Config) -> Vocabulary:
    print('Vocabulary')
    try:
        vocabulary = pkl.load(config.path.vocabulary)
    except FileNotFoundError:
        vocabulary = Vocabulary(config.path.dictionary)
        pkl.save(vocabulary, config.path.vocabulary)
    assert vocabulary.decode(vocabulary.encode('ni gdy <> nie _')) == 'ni gdy <> nie α'

    return vocabulary


def load_train_valid_dataset(config: Config, vocabulary: Vocabulary) -> tuple[Dataset, Dataset]:
    if not (os.path.isfile(config.path.train_dataset) and os.path.isfile(config.path.valid_dataset)):
        if not os.path.isfile(config.path.dataset):
            print('Dataset.tokenized2dataset')
            Dataset.tokenized2dataset(vocabulary=vocabulary, config=config)

        print('Dataset.split_train_valid')
        Dataset.split_train_valid(
            dataset_filename=config.path.dataset,
            train_filename=config.path.train_dataset,
            valid_filename=config.path.valid_dataset,
        )

    print(f'Datasets from {config.path.train_dataset}, {config.path.valid_dataset}')
    train_dataset = Dataset(filename=config.path.train_dataset, config=config)
    valid_dataset = Dataset(filename=config.path.valid_dataset, config=config)
    return train_dataset, valid_dataset
