import os
import pickle
import time
from typing import Any

import torch
from torch import Tensor, nn
# noinspection PyPep8Naming
from torch.nn import functional as F


LETTERS = list('abcdefghijklmnopqrstuvwxyzóąćęłńśźż')
PUNCTUATION = list('!(),-.:;')
PUNCTUATION_RIGHT = ['(']
HYPHEN = '-'
TOKEN_UNK = 'α'
TOKEN_EOW = '<>'
TOKEN_BR = '<br>'
SPECIAL = PUNCTUATION + [TOKEN_UNK, TOKEN_EOW, TOKEN_BR]


class Config:
    embedding_dimension = 128
    num_heads = 4
    num_layers = 3
    context_size = 32

    batch_size = 64
    num_epochs = 200
    learning_rate = 1e-4
    logging_interval = 25000

    gradient_clipping = 1.0
    dropout = 0.1
    seed = 1633
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    # noinspection PyPep8Naming, PyShadowingBuiltins
    @staticmethod
    def criterion(input: Tensor, target: Tensor) -> Tensor:
        B, C, E = input.shape
        input = input.view(B * C, E)
        target = target.view(B * C)
        return F.cross_entropy(input, target)

    # noinspection PyPep8Naming
    class path:
        # base_dir = 'data'
        base_dir = '/content/drive/MyDrive/Colab/data'

        dictionary = os.path.join(base_dir, 'dictionary.txt')
        tokenized = os.path.join(base_dir, 'tokenized.txt')
        dataset = os.path.join(base_dir, 'dataset.txt')

        vocabulary = os.path.join(base_dir, 'vocabulary.pkl')
        train_dataset = os.path.join(base_dir, 'train_dataset.txt')
        valid_dataset = os.path.join(base_dir, 'valid_dataset.txt')
        checkpoint_dir = os.path.join(base_dir, 'checkpoints')

    def __str__(self):
        return f'''class Config:
    embedding_dimension = {self.embedding_dimension}
    num_heads = {self.num_heads}
    num_layers = {self.num_layers}
    context_size = {self.context_size}
    
    batch_size = {self.batch_size}
    num_epochs = {self.num_epochs}
    learning_rate = {self.learning_rate}
    logging_interval = {self.logging_interval}
    
    gradient_clipping = {self.gradient_clipping}
    dropout = {self.dropout}
    seed = {self.seed}
    device = {self.device}'''


def detokenize(text: str) -> str:
    """
    By default, no space is inserted between tokens because tokens are syllable tokens
    """
    tokens = text.split()
    tokens.append(TOKEN_BR)
    output = ''

    for token, next_token in zip(tokens, tokens[1:]):
        if token not in SPECIAL:
            output += token
        else:
            if token == TOKEN_BR:
                output += '\n'

            elif token == TOKEN_EOW:
                if next_token not in SPECIAL:
                    output += ' '

            elif token == TOKEN_UNK:
                output += ' '
                output += token
                output += ' '

            elif token == HYPHEN:
                output += token

            elif token in PUNCTUATION_RIGHT:
                output += ' '
                output += token

            else:
                output += token
                if next_token not in SPECIAL:
                    output += ' '

    output = output.strip()
    return output


def now() -> str:
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))


def encoded2tensor(encoded: list[int]) -> Tensor:
    return torch.tensor(encoded, dtype=torch.long)


# noinspection PyPep8Naming
class pkl:
    @staticmethod
    def save(obj: Any, filename: str) -> None:
        with open(filename, 'wb') as file:
            pickle.dump(obj, file)

    @staticmethod
    def load(filename: str) -> Any:
        with open(filename, 'rb') as file:
            return pickle.load(file)


# noinspection PyPep8Naming
class checkpoint:
    @staticmethod
    def save(model: nn.Module, epoch: int, config: Config) -> None:
        checkpoint_path = os.path.join(config.path.checkpoint_dir, f'model_epoch{epoch}.pt')
        print(f'checkpoint.save {checkpoint_path}')
        torch.save(model.state_dict(), checkpoint_path)

    @classmethod
    def load(cls, model: nn.Module, config: Config) -> int:
        checkpoint_dir = config.path.checkpoint_dir
        checkpoint_files = [f for f in os.listdir(checkpoint_dir) if f.startswith('model_epoch') and f.endswith('.pt')]

        if checkpoint_files:
            checkpoint_files.sort(key=cls._epoch_num)
            latest_checkpoint = os.path.join(checkpoint_dir, checkpoint_files[-1])
            print(f'checkpoint.load {latest_checkpoint}')
            cls._load_model(model=model, filename=latest_checkpoint)
            return cls._epoch_num(latest_checkpoint)

        return 0

    @staticmethod
    def _epoch_num(filename: str) -> int:
        return int(filename.split('epoch')[1].split('.pt')[0])

    @staticmethod
    def _load_model(model: nn.Module, filename: str) -> None:
        try:
            model.load_state_dict(torch.load(filename))
        except RuntimeError as e:
            if str(e).startswith('Attempting to deserialize object on a CUDA device'):
                model.load_state_dict(torch.load(filename, map_location=torch.device('cpu')))
            else:
                raise e


if __name__ == '__main__':
    assert detokenize('je stem <> stu den tem <> in for α ty ki'), 'jestem studentem infor α tyki'
    assert detokenize('stu dent α stu den ci'), 'student α studenci'
