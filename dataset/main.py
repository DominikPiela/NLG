from AddBr import AddBr
from Join import Join
from PolishWikipediaPlaintext import PolishWikipediaPlaintext
from ReduceConsecutiveDuplicates import ReduceConsecutiveDuplicates
from ReduceEndOfWordTokens import ReduceEndOfWordTokens
from ReduceMultipleAlphas import ReduceMultipleAlphas
from RemoveEmptyLines import RemoveEmptyLines
from TokenizeDictionary import TokenizeDictionary
from TokenizeSpacy import TokenizeSpacy
from TokenizeSyllables import TokenizeSyllables
from TokenOccurrence import TokenOccurrence


def main():
    # Polish Wikipedia Corpus https://clip.ipipan.waw.pl/PolishWikipediaCorpus
    PolishWikipediaPlaintext(input_directory='plwiki3', output_directory='plwiki3.1').run()
    Join(input_directory='plwiki3.1', output_directory='plwiki3.2').run()
    TokenizeSpacy(input_directory='plwiki3.2', output_directory='plwiki3.3').run()
    TokenizeSyllables(input_directory='plwiki3.3', output_directory='plwiki3.4').run()
    TokenOccurrence(input_directory='plwiki3.4', output_directory='plwiki3.4_').run()
    TokenizeDictionary(input_directory='plwiki3.4', output_directory='plwiki3.5').run()
    ReduceEndOfWordTokens(input_directory='plwiki3.5', output_directory='plwiki3.6').run()
    ReduceMultipleAlphas(input_directory='plwiki3.6', output_directory='plwiki3.7').run()
    ReduceConsecutiveDuplicates(input_directory='plwiki3.7', output_directory='plwiki3.8').run()
    RemoveEmptyLines(input_directory='plwiki3.8', output_directory='plwiki3.9').run()
    AddBr(input_directory='plwiki3.9', output_directory='plwiki3.10').run()
    TokenOccurrence(input_directory='plwiki3.10', output_directory='plwiki3.10_').run()


if __name__ == '__main__':
    main()
