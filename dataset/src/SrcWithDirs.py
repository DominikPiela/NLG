import os

from Src import Src


class SrcWithDirs(Src):
    def process_directory(self, directory: str) -> None:
        os.makedirs(self.get_output_path(directory))
