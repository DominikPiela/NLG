from Src import Src


class Join(Src):
    def process_file(self, filename: str) -> None:
        with (open(filename, encoding='utf-8') as input_file,
              open(self.output_filename, 'a+', encoding='utf-8') as output_file):
            output_file.write(input_file.read().strip())
            output_file.write('\n\n')
