from Src import Src


class ReduceEndOfWordTokens(Src):
    SPECIAL = ['!', '(', ')', ',', '-', '.', ':', ';', 'α', '<br>']  # without '<>'

    def process_tokens(self, tokens: list[str]) -> list[str]:
        updated_tokens = []
        for prev_token, token, next_token in zip([None] + tokens[:-1], tokens, tokens[1:] + [None]):
            if token == '<>':
                if (next_token != '<>') and (prev_token not in self.SPECIAL) and (next_token not in self.SPECIAL):
                    updated_tokens.append(token)
            else:
                updated_tokens.append(token)
        return updated_tokens
