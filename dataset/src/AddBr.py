from Src import Src


class AddBr(Src):
    def process_tokens(self, tokens: list[str]) -> list[str]:
        return tokens + ['<br>']
