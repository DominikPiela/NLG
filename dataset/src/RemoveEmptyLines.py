from typing import TextIO

from Src import Src


class RemoveEmptyLines(Src):
    def process(self, input_file: TextIO, output_file: TextIO) -> None:
        for line in input_file:
            line = line.strip()
            if line != '':
                output_file.write(line + '\n')
