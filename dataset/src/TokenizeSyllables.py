import pyphen

from Src import Src


class TokenizeSyllables(Src):
    SPECIAL = ['!', '(', ')', ',', '-', '.', ':', ';', 'α', '<br>', '<>']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pyphen_dict = pyphen.Pyphen(lang='pl')

    def process_tokens(self, tokens: list[str]) -> list[str]:
        words = tokens
        words.append('<br>')

        output = []
        for word, next_word in zip(words, words[1:]):
            if word not in self.SPECIAL:
                syllables = self.syllabize(word)
                if next_word not in self.SPECIAL:
                    syllables.append('<>')
                output.extend(syllables)
            else:
                output.append(word)

        return output

    def syllabize(self, word: str) -> list[str]:
        return self.pyphen_dict.inserted(word).split('-')


if __name__ == '__main__':
    import pyphen

    dic = pyphen.Pyphen(lang='pl')
    print(dic.inserted('studentem'))
    # stu-den-tem
