from Src import Src


class ReduceConsecutiveDuplicates(Src):
    def process_tokens(self, tokens: list[str]) -> list[str]:
        reduced_tokens = []
        prev_element = None
        consecutive_count = 0

        for token in tokens:
            if token == prev_element:
                consecutive_count += 1
                if consecutive_count <= 2:
                    reduced_tokens.append(token)
            else:
                consecutive_count = 1
                reduced_tokens.append(token)
            prev_element = token

        return reduced_tokens
