from typing import TextIO

import spacy

from Src import Src


class TokenizeSpacy(Src):
    ALPHA = 'abcdefghijklmnopqrstuvwxyzóąćęłńśźż'
    PUNCTUATION = list('!(),-.:;')
    UNKNOWN = 'α'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.nlp = spacy.load('pl_core_news_lg')

    def process(self, input_file: TextIO, output_file: TextIO) -> None:
        for line in input_file:
            output_file.write(self.tokenize(line.strip()) + '\n')

    def tokenize(self, text: str) -> str:
        return ' '.join([self.process_word(token.text.lower()) for token in self.nlp.tokenizer(text)])

    def process_word(self, word: str) -> str:
        alpha = all([c in self.ALPHA for c in word])
        punctuation = word in self.PUNCTUATION
        condition = alpha or punctuation
        return word if condition else self.UNKNOWN


if __name__ == '__main__':
    import spacy

    nlp = spacy.load('pl_core_news_lg')
    doc = 'Jestem studentem.'
    print([token.text for token in nlp.tokenizer(doc)])
    # ['Jestem', 'studentem', '.']

    print(''.join(sorted(list('!(),-.:;'))))
