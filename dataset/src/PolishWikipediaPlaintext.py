from typing import TextIO

from SrcWithDirs import SrcWithDirs


class PolishWikipediaPlaintext(SrcWithDirs):
    def process(self, input_file: TextIO, output_file: TextIO) -> None:
        lines = input_file.readlines()
        output_file.writelines(lines[1:-1])
