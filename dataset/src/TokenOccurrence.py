from collections import Counter
from typing import TextIO

from Src import Src


class TokenOccurrence(Src):
    def process(self, input_file: TextIO, output_file: TextIO) -> None:
        counter = Counter()
        for line in input_file:
            tokens = line.strip().split()
            counter.update(tokens)

        n = None  # n = 12194
        for value, occurrence in counter.most_common(n):
            output_file.write(f'{value} {occurrence}\n')

        tokens = [value for value, occurrence in counter.most_common(n)]
        print(tokens)
