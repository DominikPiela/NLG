from typing import TextIO

from Src import Src


class RemoveNewlines(Src):
    def process(self, input_file: TextIO, output_file: TextIO) -> None:
        for line in input_file:
            line = line.strip()
            output_file.write(line + ' ')
