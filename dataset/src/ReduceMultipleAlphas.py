from Src import Src


class ReduceMultipleAlphas(Src):
    def process_tokens(self, tokens: list[str]) -> list[str]:
        return [token for token, next_token in zip(tokens, tokens[1:] + [None]) if token != 'α' or next_token != 'α']
