import os
from typing import TextIO


class Src:
    def __init__(self, input_directory: str, output_directory: str, filename: str = 'data'):
        self.input_directory = input_directory
        self.output_directory = output_directory
        self.input_filename = os.path.join(self.input_directory, filename)
        self.output_filename = os.path.join(self.output_directory, filename)
        self.filename = filename
        os.makedirs(output_directory)

    def run(self) -> None:
        for root, dirs, files in os.walk(self.input_directory):
            dirs.sort()
            files.sort()
            self._walk(
                dirs=[os.path.join(root, directory) for directory in dirs],
                files=[os.path.join(root, filename) for filename in files]
            )

    def _walk(self, dirs: list[str], files: list[str]) -> None:
        for directory in dirs:
            self.process_directory(directory)
        for filename in files:
            self.process_file(filename)

    def process_directory(self, directory: str) -> None:
        pass

    def process_file(self, filename: str) -> None:
        with (open(filename, encoding='utf-8') as input_file,
              open(self.get_output_path(filename), 'w+', encoding='utf-8') as output_file):
            self.process(input_file, output_file)

    def get_output_path(self, path: str) -> str:
        return path.replace(self.input_directory, self.output_directory, 1)

    def process(self, input_file: TextIO, output_file: TextIO) -> None:
        for line in input_file:
            line = line.strip()
            tokens = line.split()
            output_file.write(' '.join(self.process_tokens(tokens)) + '\n')

    def process_tokens(self, tokens: list[str]) -> list[str]:
        raise NotImplementedError
