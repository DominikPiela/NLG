# Natural Language Generation

Syllable-based natural language generation

### Structure

- `dataset/` - dataset preparation
- `model/` - model architecture
- `Notebook.ipynb` - model training

### Running

1. Download Polish Wikipedia Corpus plain text: http://clip.ipipan.waw.pl/wiki_static/large_data/clip/PolishWikipediaCorpus/Wikipedia_PL.tar.gz (`Wikipedia_PL.tar.gz`)

2. Extract `Wikipedia_PL.tar.gz` (output directory: `dataset/plwiki3/`)
```
dataset/
  plwiki3/
  src/
  main.py
  requirements.txt
```

3. Run
```bash
cd dataset
python3 main.py
```

4. Copy files from `dataset/` to `model/`
```bash
mkdir model/data/
mkdir model/data/checkpoints/
cp dataset/plwiki3.10/data model/data/tokenized.txt
cp dataset/plwiki3.10_/data model/data/dictionary.txt
```

5. `model/util.py` line 48-49 temporarily change to
```python
base_dir = 'data'  
# base_dir = '/content/drive/MyDrive/Colab/data'
```

6. Run
```bash
cd model
python3 main.py
# Ctrl+C when training begins
```

7. `model/util.py` restore line 48-49
```python
# base_dir = 'data'  
base_dir = '/content/drive/MyDrive/Colab/data'
```

8. Copy files and directories to Google Drive
```
Colab/
  data.py
  main.py
  model.py
  util.py

  data/
    train_dataset.txt
    valid_dataset.txt
    vocabulary.pkl

  checkpoints/

Colab Notebooks/
  Notebook.ipynb
```

9. Run `Notebook.ipynb` cells in Google Colab
